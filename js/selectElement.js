// Selectionner les éléments par leurs ID il prend un argument l'ID en string
console.log(document.getElementById("grostitre"));

// Selection de l'élément par l'ID + changement de celui-ci
document.getElementById("grostitre").textContent = "C'est le nouveau titre";

// Selection par les tags
console.log(document.getElementsByTagName("p"));

// Selection par les class
console.log(document.getElementsByClassName("important"));

// Stock un élément dans une variable 
const paragraphe = document.getElementsById("grostitre");
// Affiche ma variable paragraphe
console.log(paragraphe);