// Quand la page est chargée il execute la fonction annonyme
window.onload = function()
{
    // Je stock dans des variables les selecteurs 
    const parent = document.querySelector("#parent");
    const enfant = document.querySelector("#enfant");

    // Il attend le click pour déclencher la function
    // Il part de l'enfant vers le parent 
    // Le code est executer à partir de la phase de boullonnement
    enfant.addEventListener("click", enfantFonction);
    parent.addEventListener("click", parentFonction);
    
    // Il attend le click pour déclencher la function
    // Il part du parent vers l'enfant
    // Le code est effectuer pendant la phase de capture
    enfant.addEventListener("click", enfantFonction, true);
    parent.addEventListener("click", parentFonction, true);

    // Je creer les fonctions
    function enfantFonction(event)
    {
        console.log("Clic sur enfant");
    }
    function parentFonction(event)
    {
        console.log("Clic sur parent");
    }
}