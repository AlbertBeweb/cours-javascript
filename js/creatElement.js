// Créer un élément "div"
const jaune = document.createElement("div");

// Je cherche jaune avec classList
// Et je lui ajoute jaune
jaune.classList.add("jaune");
// J'ajoute le contenu dans la div qui est dans ma variable
jaune.textContent = "Jaune";

document.body.appendChild(jaune);

// Fonction qui planifie des tâches avec deux arguments
function planifierTache (heure, tache)
{
    // Je commence a creer une variable pour creer l'élément li
    const nouvelleTache = document.createElement("li");
    
    // Insertion d'une variable dans un template "STRING"
    // Alt Gr + 7 = `` : appui 2 fois
    // + ${nomVariable}
    // Creation du template
    // ex-> `<h3>${heure}</h3><p>${tache}</p>` 
    
    // Je l'insert dans innerHtml
    nouvelleTache.innerHTML = `<h3>${heure}</h3><p>${tache}</p>`;

    // Là je l'attache à mon document HTML avec querySelector
    document.querySelector("ul").appendChild(nouvelleTache);
}

planifierTache('08h00', 'boire un coup');
planifierTache('08h30', 'Faire une pause');
planifierTache('09h00', 'Re boire un coup');
planifierTache('09h30', 'Comprendra qui voudra :)');