// Affiche PI
console.log(Math.PI);
// Affiche la valeur absolue
console.log(Math.abs(-5));
// Méthode qui permet d'arondir
console.log(Math.round(8.2));
// Méthode qui permet valeur entière suppérieur
console.log(Math.ceil(8.2));
// Méthode qui permet valeur entière inférieur
console.log(Math.floor(8.2));

// Méthode qui permet 
// Elle prends autant d'arguments que l'ont veut
// Il donne le max de tous
console.log(Math.max(10,20,65));
console.log(Math.min(10,20,65));

// Nombre aleatoire entre 0 et 1
console.log(Math.random());

// Avoir un nombre aleatoire entre 0 et 100
console.log(Math.floor(Math.random()*100) + 1);