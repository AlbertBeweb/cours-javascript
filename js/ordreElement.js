// Je selectionne mes selecteurs css 
// et je les stock dans une variable
const rouge = document.querySelector(".rouge");
const vert = document.querySelector(".vert");
const bleu = document.querySelector(".bleu");

// Insertion avant avec deux arguments
// Les arguments s'écrivent dans le bon sens
document.body.insertBefore(bleu,rouge);

// Appendchild ratache un élément enfant
// Un seul argument
// En gros ils sont dans une même <div></div>
rouge.appendChild(bleu);

// Remplace un élément enfant par un autre
// Prend 2 arguments
// Premier argument l'élément que je veut mettre
// Second l'élément que je souhaite remplacer
document.body.replaceChild(rouge,vert);

// Supprime un élément
// -> bleu.parentElement.removeChild(bleu);
// Autre méthode avec le selecteur css
rouge.remove();