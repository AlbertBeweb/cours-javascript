// Je récupère l'atribut src de <img il prend un argument le nom en string de l'atribut
console.log(document.body.children[1].getAttribute("src"));

// Changer la valeur d'un atribut, il prend en argument 2 valeur l'atribut a modifier et la valeur
console.log(document.body.children[1].setAttribute("src", "./img/js.png"));