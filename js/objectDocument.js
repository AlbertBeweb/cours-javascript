// C'est une propriété de l'objet window

// Je vais chercher dans document pour toutes les informations
console.log(window);

// J'affiche que la représentaion HTML
// Affichage du DOM Document Object Model
console.log(document);

// Affichage de l'élément title dans la console
console.log(document.title);

// Modification de la balise title
document.title = "New title";

// Affichage le contenu de la balise body
console.log(document.body);

// Affichage le contenu des enfants de body, sous forme de tableau
console.log(document.body.children);

// Affichage du premier élément dans le premier enfant et donne ses enfants
console.log(document.body.children[0].children);

// Plusieurs possiblitées 
console.log(document.body.firstElementChild);
console.log(document.body.lastElementChild);
console.log(document.body.children[0]);
// Prochain élément frère
console.log(document.body.children[0].nextElementSibling);
// Elément parent
console.log(document.body.children[0].parentElement)