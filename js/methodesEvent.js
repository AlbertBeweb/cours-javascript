window.onload = function()
{
    const form = document.querySelector("form");

    form.addEventListener("submit", envoyerFormulaire);

    function envoyerFormulaire(event)
    {
        // Annule le comportement par default
        event.preventDefault();
        console.log("Formulaire Envoyé !");
    }
}