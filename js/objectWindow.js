// Création de la variable a qui est égale à Nicolas 
var a = "Nicolas";

// Création de la fontion coucou qui affiche coucou
function coucou()
{
    console.log("Coucou");
}

// Affichage de l'objet windows
console.log(window);

// Inner

// Affichage de la largeur interne de la fenêtre 
console.log(window.innerWidth);
// Affichage de la hauteur interne de la fenêtre
console.log(window.innerHeight);

// Outer

// Affichage de la largeur externe de la fenêtre
console.log(window.outerWidth);
// Affichage de la largeur externe de la fenêtre
console.log(window.outerHeight);
