// Affiche les propriétes de l'élément ciblé
console.log(document.body.children[0].style);

// Change le backgroundColor en camel case
document.body.children[0].style.backgroundColor = "red";