// Il execute la fonction une fois que le clique se produit
// Il attend le click avec addEventListerner
noir.addEventListener("click", function(event)
{
    // Ensuite il execute ce code
    noir.textContent = "C'est la tombola";
});

window.onload = function()
{
    const violet = document.querySelector(".violet");
    const noir = document.querySelector(".noir");
    const marron = document.querySelector(".marron");

    function modifieViolet(event)
    {
        violet.textContent = "Ont clique";
    }
    function modifieNoir(event)
    {
        marron.textContent = "Ont va tout faire";
    }

    violet.addEventListener("click", modifieViolet);
    violet.addEventListener("click", modifieMarron);
}