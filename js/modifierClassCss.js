// Liste toute les class CSS de l'élément pointer
console.log(document.body.children[0].classList);

// Ajouter une class CSS
document.body.children[0].classList.add("super");

// Enlever une class CSS
document.body.children[0].classList.remove("super");

// Faire le contraire avec toggle
document.body.children[0].classList.toggle("super");