// Je créer une nouvelle requête et la stock dans une variable
const req = new XMLHttpRequest();
// Je souhaite utiliser la méthod GET et la stock dans une variable
const method = "POST";
// Je défini l'url ou se trouve les informations et la stock dans une variable
const url = 'https://jsonplaceholder.typicode.com/posts';

const data = {
    body: "blabla",
    title: "Titre du post",
    userId: 16
};

// Send et open sont des méthodes de XMLHttpRequest

// Open permet d'ouvrir notre requête, elle prends 2 arguments
// La première c'est la méthode et la deuxième c'est l'url
// C'est pour ça que l'ont les défini avant et ont les stock dans des variables
req.open(method, url);

req.onreadystatechange = function (event) {
    // Si ma requête c'est bien déroulé
    if (this.readyState === XMLHttpRequest.DONE) {
        // Et que sont statut est de 200
        if (this.status === 201) {
            // J'affiche le résultat de cet objet
            // JSON.parse permet de le formater en objet JavaScript
            console.log(JSON.parse(this.responseText));
            // Sinon j'affiche l'objet et son erreur
        } else {
            console.log("Statut : " + this.status);
        }
    }
};

// Ont envoie cette requête
req.send(data);