// Je selectionne mes selecteurs css 
// et je les stock dans une variable
const rouge = document.querySelector(".rouge");
const vert = document.querySelector(".vert");
const bleu = document.querySelector(".bleu");

// Acces à toutes les propriétées 
console.log(rouge.parentElement.children);


// Ici c'est une fonction annonyme
window.onload = function()
{
    // Ce code s'execute qu'une fois que la page est totalement chargée
    console.log("La page est chargée !");
}
// Ce console est chargé avant !!!!
console.log("Coucou");



/////////////////////////////////////////////////////////////////////////

// La même fonction déclarée
window.onload = windowReady;

function windowReady()
{
    console.log("La page est chargée");
}

window.onload = function()
{
    const rouge = document.querySelector(".violet");
    const vert = document.querySelector(".noir");
    const bleu = document.querySelector(".marron");
}

violet.onclick = function()
{
    noir.textContent = "Ca change";
}




